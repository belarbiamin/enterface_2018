
#11/07/2018 Mohammed Amin BELARBI; mohammedamin.belarbi@umons.ac.be
# In[ ]:


# coding: utf-8

# In[ ]:


from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import decode_predictions
from keras.applications import vgg16
from keras.applications import vgg19
from keras.applications import resnet50
from keras.applications import inception_v3
from keras.applications import mobilenet
from keras.applications import xception
from scipy.misc import imread, imresize
import matplotlib.pyplot as plt
import numpy as np
import operator
import math
import os

# load the model
model1 = vgg16.VGG16(weights='imagenet', include_top=True,pooling='avg')
model2 = vgg19.VGG19(weights='imagenet', include_top=True,pooling='avg')
model3 = inception_v3.InceptionV3(weights='imagenet', include_top=False, pooling='avg')
#model4 = mobilenet.MobileNet(weights='imagenet', include_top=False, pooling='avg')
model5 = resnet50.ResNet50(weights='imagenet', include_top=False, pooling='avg')
model6= xception.Xception(weights='imagenet', include_top=False, pooling='avg')

files = os.listdir('/shared/databases/ILSVRC/Data/CLS-LOC/val/')
features1 = []
features2 = []
features3 = []
features5 = []
features6 = []

pas =0
for i in files :
    data = '/shared/databases/ILSVRC/Data/CLS-LOC/val/'+str(i)
    file_name = os.path.basename(data)
    # load an image from file
    
    image = load_img(data, target_size=(224, 224))
    # convert the image pixels to a numpy array
    
    image = img_to_array(image)
    # reshape data for the model
    
    image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
    # prepare the image for the VGG model
    
    image = preprocess_input(image)
    # predict the probability across all output classes
    
    # Model 1
    feature = model1.predict(image)
    feature = np.array(feature[0])  
    np.savetxt("Features/VGG16/"+os.path.splitext(file_name)[0]+".txt",feature)
    features1.append((data,feature))
   
    # Model 2
    feature = model2.predict(image)
    feature = np.array(feature[0])
    np.savetxt("Features/VGG19/"+os.path.splitext(file_name)[0]+".txt",feature)
    features2.append((data,feature))
    
    # Model 3
    feature = model3.predict(image)
    feature = np.array(feature[0])
    np.savetxt("Features/InceptionV3/"+os.path.splitext(file_name)[0]+".txt",feature)
    features3.append((data,feature))
    
    # Model 4
    #feature = model4.predict(image)
    #feature = np.array(feature[0])
    #features4.append((data,feature))
    
    # Model 5
    feature = model5.predict(image)
    feature = np.array(feature[0])
    np.savetxt("Features/ResNet50/"+os.path.splitext(file_name)[0]+".txt",feature)
    features5.append((data,feature))
    
    # Model 6
    feature = model6.predict(image)
    feature = np.array(feature[0])
    np.savetxt("Features/Xception/"+os.path.splitext(file_name)[0]+".txt",feature)
    features6.append((data,feature))
    print (pas)
    pas = pas+1
    
        
def euclidianDistance(l1,l2):
    distance = 0
    length = min(len(l1),len(l2))
    for i in range(length):
        distance += pow((l1[i] - l2[i]), 2)
    return math.sqrt(distance)

def getkVoisins(lfeatures, test, k) : #Renvoit un liste des k voisions les plus proches de la forme (num_image, feature, k)
    ldistances = []
    for i in range(len(lfeatures)):
        dist = euclidianDistance(test[1], lfeatures[i][1])
        ldistances.append((lfeatures[i][0], lfeatures[i][1], dist))
    ldistances.sort(key=operator.itemgetter(2))
    lvoisins = []
    for i in range(k):
        lvoisins.append(ldistances[i])
    return lvoisins

#Classification part
#label = decode_predictions(yhat)
# retrieve the most likely result, e.g. highest probability
#label = label[0][0]
# print the classification
#print('%s (%.2f%%)' % (label[1], label[2]*100))

#Save file
with open("Features/VGG16.txt", "w") as output:
    output.write(str(features1))
    
with open("Features/VGG19.txt", "w") as output:
    output.write(str(features2))

with open("Features/InceptionV3.txt", "w") as output:
    output.write(str(features3))
    
#with open("features4.txt", "w") as output:
#    output.write(str(features4))
    
with open("Features/ResNet50.txt", "w") as output:
    output.write(str(features5))
    
with open("Features/Xception.txt", "w") as output:
    output.write(str(features6))


voisins = getkVoisins(features1, features1[0],10)
#Affichage du voisin le plus proche, avec sa distance associ�e
#print(voisins[0])

# AlexNet
nom_image_plus_proches = [] #num�ros images plus proches
for k in range(10):
    nom_image_plus_proches.append(voisins[k][0])
plt.figure()
plt.subplot(1,3,1)
plt.imshow(imread(features1[0][0]), cmap='gray', interpolation='none')
plt.title("Image requ�te")
for j in range(10): 
    plt.figure()
    plt.subplot(211)
    #plt.imshow(X_test[num_image_plus_proche], cmap='gray', interpolation='none')
    plt.imshow(imread(nom_image_plus_proches[j]), cmap='gray', interpolation='none')
    #plt.imsave("image"+str(j),images[num_image_plus_proches[j]])
    title = "Image la plus proche n�"+str(j)
    plt.title(title)

