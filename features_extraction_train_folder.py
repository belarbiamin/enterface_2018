# coding: utf-8

# In[ ]:


from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import decode_predictions
from keras.applications import vgg16
from keras.applications import vgg19
from keras.applications import resnet50
from keras.applications import inception_v3
from keras.applications import mobilenet
from keras.applications import xception
from scipy.misc import imread, imresize
import matplotlib.pyplot as plt
import numpy as np
import operator
import math
import os
import tensorflow as tf
sess = tf.Session()

# load the model
model1 = vgg16.VGG16(weights='imagenet', include_top=True,pooling='avg')
model2 = vgg19.VGG19(weights='imagenet', include_top=True,pooling='avg')
model3 = inception_v3.InceptionV3(weights='imagenet', include_top=False, pooling='avg')
#model4 = mobilenet.MobileNet(weights='imagenet', include_top=False, pooling='avg')
model5 = resnet50.ResNet50(weights='imagenet', include_top=False, pooling='avg')
model6= xception.Xception(weights='imagenet', include_top=False, pooling='avg')

files = "/scratch/belarbima/ILSVRC/Data/CLS-LOC/train/"
features1 = []
features2 = []
features3 = []
features5 = []
features6 = []

big_folder="/scratch/belarbima/Features_train/"
if not os.path.exists(big_folder):
    os.makedirs(big_folder)


folder_model1="/scratch/belarbima/Features_train/VGG16/"
folder_model2="/scratch/belarbima/Features_train/VGG19/"
folder_model3="/scratch/belarbima/Features_train/InceptionV3/"
folder_model5="/scratch/belarbima/Features_train/ResNet50/"
folder_model6="/scratch/belarbima/Features_train/Xception/"

if not os.path.exists(folder_model1):
    os.makedirs(folder_model1)
if not os.path.exists(folder_model2):
    os.makedirs(folder_model2)
if not os.path.exists(folder_model3):
    os.makedirs(folder_model3)
if not os.path.exists(folder_model5):
    os.makedirs(folder_model5)
if not os.path.exists(folder_model6):
    os.makedirs(folder_model6)

pas =0

for j in os.listdir(files) :
    sous_files = os.path.join(files, j)
    dossier1=folder_model1+str(j)
    dossier2=folder_model2+str(j)
    dossier3=folder_model3+str(j)
    dossier5=folder_model5+str(j)
    dossier6=folder_model6+str(j)
    if not os.path.exists(dossier1) :
        os.makedirs(dossier1)
    if not os.path.exists(dossier2) :
        os.makedirs(dossier2)
    if not os.path.exists(dossier3) :
        os.makedirs(dossier3)
    if not os.path.exists(dossier5) :
        os.makedirs(dossier5)
    if not os.path.exists(dossier6) :
        os.makedirs(dossier6)
    print (sous_files)
    for i in os.listdir(sous_files) :
        data = os.path.join(sous_files, i)
        #data = sous_files+"/"+str(i)
        if not data.endswith(".JPEG"):
            continue
        print(i)
        file_name = os.path.basename(data)
        # load an image from file

        image = load_img(data, target_size=(224, 224))
        # convert the image pixels to a numpy array

        image = img_to_array(image)
        # reshape data for the model

        image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
        # prepare the image for the VGG model

        image = preprocess_input(image)
        # predict the probability across all output classes

        # Model 1
        feature = model1.predict(image)
        feature = np.array(feature[0])  
        np.savetxt(dossier1+"/"+os.path.splitext(file_name)[0]+".txt",feature)
        features1.append((data,feature))

        # Model 2
        feature = model2.predict(image)
        feature = np.array(feature[0])
        np.savetxt(dossier2+"/"+os.path.splitext(file_name)[0]+".txt",feature)
        features2.append((data,feature))

        # Model 3
        feature = model3.predict(image)
        feature = np.array(feature[0])
        np.savetxt(dossier3+"/"+os.path.splitext(file_name)[0]+".txt",feature)
        features3.append((data,feature))

        # Model 4
        #feature = model4.predict(image)
        #feature = np.array(feature[0])
        #features4.append((data,feature))

        # Model 5
        feature = model5.predict(image)
        feature = np.array(feature[0])
        np.savetxt(dossier5+"/"+os.path.splitext(file_name)[0]+".txt",feature)
        features5.append((data,feature))

        # Model 6
        feature = model6.predict(image)
        feature = np.array(feature[0])
        np.savetxt(dossier6+"/"+os.path.splitext(file_name)[0]+".txt",feature)
        features6.append((data,feature))
        print (pas)
        pas = pas+1
    
        
def euclidianDistance(l1,l2):
    distance = 0
    length = min(len(l1),len(l2))
    for i in range(length):
        distance += pow((l1[i] - l2[i]), 2)
    return math.sqrt(distance)

def getkVoisins(lfeatures, test, k) : #Renvoit un liste des k voisions les plus proches de la forme (num_image, feature, k)
    ldistances = []
    for i in range(len(lfeatures)):
        dist = euclidianDistance(test[1], lfeatures[i][1])
        ldistances.append((lfeatures[i][0], lfeatures[i][1], dist))
    ldistances.sort(key=operator.itemgetter(2))
    lvoisins = []
    for i in range(k):
        lvoisins.append(ldistances[i])
    return lvoisins

#Classification part
#label = decode_predictions(yhat)
# retrieve the most likely result, e.g. highest probability
#label = label[0][0]
# print the classification
#print('%s (%.2f%%)' % (label[1], label[2]*100))

#Save file
with open("/scratch/belarbima/Features_train/VGG16.txt", "w") as output:
    output.write(str(features1))
    
with open("/scratch/belarbima/Features_train/VGG19.txt", "w") as output:
    output.write(str(features2))

with open("/scratch/belarbima/Features_train/InceptionV3.txt", "w") as output:
    output.write(str(features3))
    
#with open("features4.txt", "w") as output:
#    output.write(str(features4))
    
with open("/scratch/belarbima/Features_train/ResNet50.txt", "w") as output:
    output.write(str(features5))
    
with open("/scratch/belarbima/Features_train/Xception.txt", "w") as output:
    output.write(str(features6))


